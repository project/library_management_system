<?php

/**
* Implements hook_views_data_alter().
*/
function library_management_system_views_data_alter(array &$data) {
	$data['lmspublication']['featured_image'] = array(
		'title' => t('LmsPublication Featured Image'),
		'field' => array(
			'title' => t('LmsPublication Featured Image'),
			'help' => t('LmsPublication Featured Image'),
			'id' => 'featured_image',
		),
	);

	$data['lmsbook']['featured_image'] = array(
		'title' => t('LmsBook Featured Image'),
		'field' => array(
			'title' => t('LmsBook Featured Image'),
			'help' => t('LmsBook Featured Image'),
			'id' => 'featured_image',
		),
	);
	$data['lmspublication']['related_tags'] = array(
		'title' => t('LmsPublication Related Tags'),
		'field' => array(
			'title' => t('LmsPublication Related Tags'),
			'help' => t('LmsPublication Related Tags'),
			'id' => 'related_tags',
		),
	);

	$data['lmsbook']['related_tags'] = array(
		'title' => t('LmsBook Related Tags'),
		'field' => array(
			'title' => t('LmsBook Related Tags'),
			'help' => t('LmsBook Related Tags'),
			'id' => 'related_tags',
		),
	);

	$data['lmsbookauthor']['author_books_count'] = array(
		'title' => t('Author Books Count'),
		'field' => array(
			'title' => t('Author Books Count'),
			'help' => t('Author Books Count'),
			'id' => 'author_books_count',
		),
	);

	$data['lmsbook']['book_authors'] = array(
		'title' => t('Book Authors'),
		'field' => array(
			'title' => t('Book Authors'),
			'help' => t('Book Authors'),
			'id' => 'book_authors',
		),
	);

	$data['lmsbook']['total_issued_books'] = array(
		'title' => t('Total Issued Books'),
		'field' => array(
			'title' => t('Total Issued Books'),
			'help' => t('Total Issued Books'),
			'id' => 'total_issued_books',
		),
	);
	$data['lmspublication']['total_publication_books'] = array(
		'title' => t('Total Publication Books'),
		'field' => array(
			'title' => t('Total Publication Books'),
			'help' => t('Total Publication Books'),
			'id' => 'total_publication_books',
		),
	);
	$data['requestedlmsbook']['requested_book_issued_date'] = array(
		'title' => t('Issued Date'),
		'field' => array(
			'title' => t('Issued Date'),
			'help' => t('Issued Date'),
			'id' => 'requested_book_issued_date',
		),
	);
	$data['requestedlmsbook']['requested_book_returned_date'] = array(
		'title' => t('Returned Date'),
		'field' => array(
			'title' => t('Returned Date'),
			'help' => t('Returned Date'),
			'id' => 'requested_book_returned_date',
		),
	);

	$data['lmsbook']['total_fine_amount'] = array(
		'title' => t('Total Fine Amount'),
		'field' => array(
			'title' => t('Total Fine Amount'),
			'help' => t('Total Fine Amount'),
			'id' => 'total_fine_amount',
		),
	);

	$data['requestedlmsbook']['featured_image'] = array(
		'title' => t('LmsBook Featured Image'),
		'field' => array(
			'title' => t('LmsBook Featured Image'),
			'help' => t('LmsBook Featured Image'),
			'id' => 'featured_image',
		),
	);

	$data['node']['node_active_title'] = array(
		'title' => t('Node Active Title'),
		'field' => array(
			'title' => t('Node Active Title'),
			'help' => t('Add acrive class to node link on the same page'),
			'id' => 'node_active_title',
		),
	);
}