<?php

namespace Drupal\library_management_system;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of IssuedLmsBook entities.
 *
 * @ingroup library_management_system
 */
class IssuedLmsBookListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Issued Book ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\library_management_system\Entity\IssuedLmsBook */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.issuedlmsbook.edit_form',
      ['issuedlmsbook' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
