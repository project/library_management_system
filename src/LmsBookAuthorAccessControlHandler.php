<?php

namespace Drupal\library_management_system;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the LmsBookAuthor entity.
 *
 * @see \Drupal\library_management_system\Entity\LmsBookAuthor.
 */
class LmsBookAuthorAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\library_management_system\Entity\LmsBookAuthorInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished lmsbookauthor entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published lmsbookauthor entities');

      case 'update':
      return AccessResult::allowedIfHasPermission($account, 'edit lmsbookauthor entities');

      case 'delete':
      return AccessResult::allowedIfHasPermission($account, 'delete lmsbookauthor entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add lmsbookauthor entities');
  }

}
