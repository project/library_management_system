<?php

namespace Drupal\library_management_system;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for issuedlmsbook.
 */
class IssuedLmsBookTranslationHandler extends ContentTranslationHandler {

}
