<?php

namespace Drupal\library_management_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting LmsPublication entities.
 *
 * @ingroup library_management_system
 */
class IssuedLmsBookDeleteForm extends ContentEntityDeleteForm {


}
