<?php

namespace Drupal\requestedlmsbook\Form;

use Drupal\Core\Entity\Form\DeleteMultipleForm as EntityDeleteMultipleForm;
use Drupal\Core\Url;

/**
 * Provides the requestedlmsbook multiple delete confirmation form.
 *
 * @internal
 */
class ConfirmDeleteMultipleRequestedLmsBook extends EntityDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->selection), 'Are you sure you want to delete this requestedlmsbook and all its children?', 'Are you sure you want to delete these requestedlmsbooks and all their children?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('requestedlmsbook.lmspublication_list');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletedMessage($count) {
    return $this->formatPlural($count, 'Deleted @count requestedlmsbook.', 'Deleted @count requestedlmsbooks.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getInaccessibleMessage($count) {
    return $this->formatPlural($count, "@count requestedlmsbook has not been deleted because you do not have the necessary permissions.", "@count requestedlmsbooks have not been deleted because you do not have the necessary permissions.");
  }

}
