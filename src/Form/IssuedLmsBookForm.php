<?php

namespace Drupal\library_management_system\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for IssuedLmsBook edit forms.
 *
 * @ingroup library_management_system
 */
class IssuedLmsBookForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\library_management_system\Entity\IssuedLmsBook */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created the %label IssuedLmsBook.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved the %label IssuedLmsBook.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.issuedlmsbook.canonical', ['issuedlmsbook' => $entity->id()]);
  }

}
