<?php

namespace Drupal\library_management_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting RequestedLmsBook entities.
 *
 * @ingroup library_management_system
 */
class RequestedLmsBookDeleteForm extends ContentEntityDeleteForm {


}
