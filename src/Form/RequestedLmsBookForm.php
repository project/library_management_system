<?php

namespace Drupal\library_management_system\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for RequestedLmsBook edit forms.
 *
 * @ingroup library_management_system
 */
class RequestedLmsBookForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\library_management_system\Entity\RequestedLmsBook */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created the %label RequestedLmsBook.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved the %label RequestedLmsBook.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.requestedlmsbook.canonical', ['requestedlmsbook' => $entity->id()]);
  }

}
