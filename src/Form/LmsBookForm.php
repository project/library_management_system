<?php

namespace Drupal\library_management_system\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for LmsBook edit forms.
 *
 * @ingroup library_management_system
 */
class LmsBookForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\library_management_system\Entity\LmsBook */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
      \Drupal::messenger()->addMessage($this->t('Created the %label LmsBook.', [
        '%label' => $entity->label(),
      ]));
      break;

      default:
      \Drupal::messenger()->addMessage($this->t('Saved the %label LmsBook.', [
        '%label' => $entity->label(),
      ]));
    }
    $form_state->setRedirect('entity.lmsbook.canonical', ['lmsbook' => $entity->id()]);
  }

}
