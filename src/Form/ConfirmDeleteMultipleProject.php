<?php

namespace Drupal\lmsbook\Form;

use Drupal\Core\Entity\Form\DeleteMultipleForm as EntityDeleteMultipleForm;
use Drupal\Core\Url;

/**
 * Provides the lmsbook multiple delete confirmation form.
 *
 * @internal
 */
class ConfirmDeleteMultipleLmsBook extends EntityDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->selection), 'Are you sure you want to delete this lmsbook and all its children?', 'Are you sure you want to delete these lmsbooks and all their children?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('lmsbook.lmspublication_list');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletedMessage($count) {
    return $this->formatPlural($count, 'Deleted @count lmsbook.', 'Deleted @count lmsbooks.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getInaccessibleMessage($count) {
    return $this->formatPlural($count, "@count lmsbook has not been deleted because you do not have the necessary permissions.", "@count lmsbooks have not been deleted because you do not have the necessary permissions.");
  }

}
