<?php

// /**
//  * @file
//  * Contains \Drupal\library_management_system\Form\EntitiesSearchForm.
//  */

// namespace Drupal\library_management_system\Form;

// use Drupal\Core\Form\FormBase;

// class EntitiesSearchForm extends FormBase {

//   /**
//    * {@inheritdoc}.
//    */
//   public function getFormId() {
//     return 'library_management_system_search_form';
//   }

//   /**
//    * {@inheritdoc}.
//    */
//   public function buildForm(array $form, array &$form_state) {

//     $form['s'] = array(
//       '#type' => 'textfield',
//       '#title' => $this->t('Keyword')
//     );
//     $form['show'] = array(
//       '#type' => 'submit',
//       '#value' => $this->t('Submit'),
//     );

//     return $form;
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function validateForm(array &$form, array &$form_state) {
//     // $values = $form_state->getValues();
//     // if (strpos($values['email'], '.com') === FALSE ) {
//     //   $form_state->setErrorByName('email', t('This is not a .com email address.'));
//     // }
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function submitForm(array &$form, array &$form_state) {
//     drupal_set_message($this->t('Your email address is @email', array('@email' => $form_state['values']['email'])));
//   }

// }














namespace Drupal\library_management_system\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the search form for the search block.
 *
 * @internal
 */
class EntitiesSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'library_management_system_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_id = NULL) {

    $form['#method'] = 'get';

    $form['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#title_display' => 'invisible',
      '#size' => 15,
      '#default_value' => $_GET['search']??'',
      '#attributes' => ['title' => $this->t('Enter the terms you wish to search for.')],
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      // Prevent op from showing up in the query string.
      '#name' => '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This form submits to the search page, so processing happens there.
  }

}
