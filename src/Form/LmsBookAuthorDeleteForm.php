<?php

namespace Drupal\library_management_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting LmsBookAuthor entities.
 *
 * @ingroup library_management_system
 */
class LmsBookAuthorDeleteForm extends ContentEntityDeleteForm {


}
