<?php

namespace Drupal\library_management_system\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines Exchange Group settings form.
 */
class FineAmountSettingsForm extends ConfigFormBase {

  /**
   * Determines the ID of a form.
   */
  public function getFormId() {
    return 'fine_configuration';
  }

  /**
   * Gets the configuration names that will be editable.
   */
  public function getEditableConfigNames() {
    return [
      'fine.configuration',
    ];
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('fine.configuration');
    $form['fine_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fine amount per day configuration'),
      '#id' => 'fine_settings',
    ];

    $form['fine_settings']['fine_per_day'] = array(
      '#id' => 'fine_per_day',
      '#type' => 'number',
      '#step' => '.01',
      '#title' => $this->t('Fine Amout'),
      '#default_value' => ($config->get('fine_per_day'))?$config->get('fine_per_day'):'',
      '#size' => 60,
      '#description' => $this->t('Fine amount per day'),
    );

    return parent::buildForm($form, $form_state);

  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('fine.configuration')
    ->set('fine_per_day', $form_state->getValue('fine_per_day'))
    ->save();
  }
}

