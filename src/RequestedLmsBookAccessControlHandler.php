<?php

namespace Drupal\library_management_system;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the RequestedLmsBook entity.
 *
 * @see \Drupal\library_management_system\Entity\RequestedLmsBook.
 */
class RequestedLmsBookAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\library_management_system\Entity\RequestedLmsBookInterface $entity */
    switch ($operation) {
      case 'view':
        // if (!$entity->isPublished()) {
        //   return AccessResult::allowedIfHasPermission($account, 'view unpublished requestedlmsbook entities');
        // }

      $owner_id = $entity->getOwnerId();
      $current_uid = $account->id();

        if($owner_id == $current_uid) {
          return AccessResult::allowed();
        } else {
        return AccessResult::allowedIfHasPermission($account, 'view published requestedlmsbook entities');
        }

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit requestedlmsbook entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete requestedlmsbook entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add requestedlmsbook entities');
  }

}
