<?php

namespace Drupal\library_management_system;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the LmsPublication entity.
 *
 * @see \Drupal\library_management_system\Entity\LmsPublication.
 */
class LmsPublicationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\library_management_system\Entity\LmsPublicationInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          // return AccessResult::allowedIfHasPermission($account, 'view unpublished lmspublication entities');

          return AccessResult::allowed();
        }
        return AccessResult::allowedIfHasPermission($account, 'view published lmspublication entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit lmspublication entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete lmspublication entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add lmspublication entities');
  }

}
