<?php

namespace Drupal\library_management_system;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the LmsBook entity.
 *
 * @see \Drupal\library_management_system\Entity\LmsBook.
 */
class LmsBookAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    /** @var \Drupal\library_management_system\Entity\LmsBookInterface $entity */
    switch ($operation) {
      case 'view':
        // if (!$entity->isPublished()) {
        //   return AccessResult::allowedIfHasPermission($account, 'view unpublished lmsbook entities');
        // }
        // return AccessResult::allowedIfHasPermission($account, 'view published lmsbook entities');

         return AccessResult::allowed();

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit lmsbook entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete lmsbook entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add lmsbook entities');
  }

}
