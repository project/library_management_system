<?php

namespace Drupal\library_management_system;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of LmsPublication entities.
 *
 * @ingroup library_management_system
 */
class LmsPublicationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Publication ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\library_management_system\Entity\LmsPublication */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.lmspublication.edit_form',
      ['lmspublication' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
