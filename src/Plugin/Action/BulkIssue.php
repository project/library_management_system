<?php

namespace Drupal\library_management_system\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;


/**
 * Action description.
 *
 * @Action(
 *   id = "library_management_system_bulk_issue",
 *   label = @Translation("Requested book bulk issue"),
 *   type = "requestedlmsbook"
 * )
 */
class Bulkissue extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $entity->setStatus(true);
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // If certain fields are updated, access should be checked against them as well.
    // @see Drupal\Core\Field\FieldUpdateActionBase::access().
    return $object->access('update', $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
   return $form;
 }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm1(array &$form, FormStateInterface $form_state) {}
}