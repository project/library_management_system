<?php

namespace Drupal\library_management_system\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;


/**
 * Action description.
 *
 * @Action(
 *   id = "library_management_system_bulk_excel",
 *   label = @Translation("Export to Excel File"),
 *   type = ""
 * )
 */
class BulkExportExcel extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $this->executeMultiple([$entity]);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // If certain fields are updated, access should be checked against them as well.
    // @see Drupal\Core\Field\FieldUpdateActionBase::access().
    return $object->access('update', $account, $return_as_object);
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }
  public function buildConfigurationForm2(array $form, FormStateInterface $form_state) {
    $context = $this->context;

    $form_state->set('context', $context);

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

// Filter Customer Data
  function filterCustomerData(&$str) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
  }

 /**
  * {@inheritdoc}
  */
 public function executeMultiple(array $entities) {
  if(count($entities) > 0) {
    $file = File::create([
      'uid' => 1,
      'filename' => 'export-'.time().'.xlsx',
      'uri' => 'public://page/export-'.time().'.xlsx',
      'status' => 0,
    ]);
    $file->save();

    $dir = dirname($file->getFileUri());
    if (!file_exists($dir)) {
      mkdir($dir, 0777, TRUE);
    }

    $download_content = array();
    $content_data = '';
    ini_set("memory_limit", "800M");
    ini_set("max_execution_time", "800");
    $options = ['absolute' => TRUE];
    $data = array();

    //To define column name in first row.
    $column_names = false;

    $excel_data = '';

    foreach ($entities as $delta => $entity) {
      $row = array();
      if(method_exists($entity,'getExcelData')) {
        $row = $entity->getExcelData();
        if(!$column_names) {
          $excel_data .= implode("\t", array_keys($row)) . "\n";
          $column_names = true;
        }
      }
      // The array_walk() function runs each array element in a user-defined function.
      array_walk($row, array($this, 'filterCustomerData'));

      $excel_data .= implode("\t", array_values($row)) . "\n";
    }

    file_put_contents($file->getFileUri(), $excel_data);

    $file->save();

    $url = file_create_url($file->getFileUri());

    $message = 'click here to download the excel file <a href='.$url.'>Download</a>';
    $rendered_message = \Drupal\Core\Render\Markup::create($message);
    \Drupal::messenger()->addMessage($rendered_message);

  }
}
}