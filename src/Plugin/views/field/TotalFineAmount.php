<?php
namespace Drupal\library_management_system\Plugin\views\field;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\file\Entity\File;
use Drupal\library_management_system\Entity\LmsPublication;
use Drupal\library_management_system\Entity\LmsBook;
use Drupal\library_management_system\Entity\LmsBookAuthor;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Render\Markup;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("total_fine_amount")
 */
class TotalFineAmount extends FieldPluginBase
{
  /**
   * {@inheritdoc}
   */
  public function usesGroupBy()
  {
    return FALSE;
  }
  /**
   * {@inheritdoc}
   */
  public function query()
  {
    // Do nothing -- to override the parent query.
  }
  /**
   * {@inheritdoc}
   */
  protected function defineOptions()
  {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values)
  {
    $item = $values->_entity;
    $bundle = $item->bundle();
    $total_fine_amount = '0';
    switch ($bundle) {
      case 'issuedlmsbook':
      $created = $item->created->getValue();
      $due_date = $item->due_date->getValue();
      $returned_date = $item->returned_date->getValue();
      $fine_price_perday = $item->fine_price->getValue();
      $created = $created[0]['value']??'';
      $due_date = $due_date[0]['value']??'';
      $returned_date = $returned_date[0]['value']??'';
      $fine_price_perday = $fine_price_perday[0]['value']??'';
      $timestamp = ($due_date != '')?strtotime($due_date):time();
      $current_timestamp = ($returned_date != '')?strtotime($returned_date):time();
      if($current_timestamp > $timestamp) {
        $diff = $timestamp - $current_timestamp;
        $no_of_days =  ceil(abs($diff / 86400));
        $total_fine_amount = $fine_price_perday*$no_of_days;
      }
      break;
    }
    return $total_fine_amount;
  }
}