<?php
namespace Drupal\library_management_system\Plugin\views\field;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\file\Entity\File;
use Drupal\library_management_system\Entity\LmsPublication;
use Drupal\library_management_system\Entity\LmsBook;
use Drupal\library_management_system\Entity\LmsBookAuthor;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Render\Markup;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("book_authors")
 */
class BookAuthors extends FieldPluginBase
{
  /**
   * {@inheritdoc}
   */
  public function usesGroupBy()
  {
    return FALSE;
  }
  /**
   * {@inheritdoc}
   */
  public function query()
  {
    // Do nothing -- to override the parent query.
  }
  /**
   * {@inheritdoc}
   */
  protected function defineOptions()
  {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values)
  {
   $item = $values->_entity;
   $bundle = $item->bundle();
   $data = '';
   switch ($bundle) {
    case 'lmsbook':

    $uids = $item->uid->getValue();

    $markup_links = [];

    if(!empty($uids)) {
      foreach ($uids as $key => $uid) {
        $target_id = $uid['target_id']??'';
        if($target_id) {
          $LmsBookAuthor = LmsBookAuthor::load($target_id);

          if($LmsBookAuthor) {
           $markup = $LmsBookAuthor->toLink();
           $link_data = $markup->toRenderable();

           $link_data['#prefix'] = "<div>";
           $link_data['#suffix'] = "</div>";
           array_push($markup_links, $link_data);
         }

       }
     }

   }

   if(!empty($markup_links)) {
     $data = $markup_links;
   }

   break;


   default:
   $data = '';
   break;
 }

 if ($data == null) {
  $data = '';
}

return $data;
}
}