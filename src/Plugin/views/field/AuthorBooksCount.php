<?php
namespace Drupal\library_management_system\Plugin\views\field;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\file\Entity\File;
use Drupal\library_management_system\Entity\LmsPublication;
use Drupal\library_management_system\Entity\LmsBook;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Render\Markup;
use \Drupal\Core\Url;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("author_books_count")
 */
class AuthorBooksCount extends FieldPluginBase
{
  /**
   * {@inheritdoc}
   */
  public function usesGroupBy()
  {
    return FALSE;
  }
  /**
   * {@inheritdoc}
   */
  public function query()
  {
    // Do nothing -- to override the parent query.
  }
  /**
   * {@inheritdoc}
   */
  protected function defineOptions()
  {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values)
  {
    $item = $values->_entity;
    $bundle = $item->bundle();
    $count = 0;
    switch ($bundle) {
      case 'lmsbookauthor':
      $user_id = $item->id();
      $query = \Drupal::entityQuery('lmsbook')
      ->accessCheck(TRUE)
      ->condition('uid', $user_id);
      $total_count = $query->count()->execute();
      $author_name = $item->getTitle();
      $url = Url::fromRoute('view.lmsbook_admin.view_all_books');
      if($url) {
        $url_link = $url->toString();
        $url_link = $url_link."?author=".$author_name;
        $markup = '<a title="View Books" target="_blank" href ="'.$url_link.'">'.$total_count.'</a> ';
        $count = Markup::create($markup);
      }
      break;
    }

    return $count;
  }
}