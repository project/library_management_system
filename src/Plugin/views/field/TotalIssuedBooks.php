<?php
namespace Drupal\library_management_system\Plugin\views\field;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\file\Entity\File;
use Drupal\library_management_system\Entity\LmsPublication;
use Drupal\library_management_system\Entity\LmsBook;
use Drupal\library_management_system\Entity\LmsBookAuthor;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Render\Markup;
use \Drupal\Core\Url;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("total_issued_books")
 */
class TotalIssuedBooks extends FieldPluginBase
{
  /**
   * {@inheritdoc}
   */
  public function usesGroupBy()
  {
    return FALSE;
  }
  /**
   * {@inheritdoc}
   */
  public function query()
  {
    // Do nothing -- to override the parent query.
  }
  /**
   * {@inheritdoc}
   */
  protected function defineOptions()
  {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values)
  {
    $item = $values->_entity;
    $bundle = $item->bundle();
    $count = 0;
    switch ($bundle) {
      case 'lmsbook':
      $book_id = $item->id();
      $total_copies = $item->getNoOfCopies();
      $query = \Drupal::entityQuery('issuedlmsbook')
      ->accessCheck(TRUE)
      ->condition('lmsbook', $book_id);
      $total_count = $query->count()->execute();
      $book_name = $item->getTitle();
      $url = Url::fromRoute('view.issuedlmsbook_admin.issued_books');
      if($url) {
        $remaining_books = 0;
        if(is_numeric($total_copies) && $total_copies > 0) {
          $remaining_books = $total_copies - $total_count;
        }
        $url_link = $url->toString();
        $url_link = $url_link."?book_name=".$book_name;
        $markup = '<a title="View Books" target="_blank" href ="'.$url_link.'">'.$total_count.' (Books Issued)</a><br> '.$remaining_books.' (Books in Library)';
        $count = Markup::create($markup);
      }
      break;
    }
    return $count;
  }
}