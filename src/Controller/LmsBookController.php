<?php

namespace Drupal\library_management_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\library_management_system\LmsBookInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\library_management_system\Entity\LmsBookAuthor;
use Drupal\library_management_system\Entity\LmsBook;
use Drupal\library_management_system\Entity\RequestedLmsBook;
use Drupal\news_article\Entity\NewsArticle;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns responses for lmsbook module routes.
 */
class LmsBookController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\library_management_system\LmsBookInterface $lmsbook
   *   The lmsbook entity.
   *
   * @return string
   *   The lmsbook label.
   */
  public function lmsbookTitle(LmsBook $lmsbook) {
    return $lmsbook->label();
  }

  /**
   * Request a book
   * @param   $lmsbook
   * @return
   */
  function request($lmsbook) {
    $lmsbook_data = array();
    $current_uid = \Drupal::currentUser()->id();

    $query = \Drupal::entityQuery('requestedlmsbook')
    ->accessCheck(TRUE)
    ->condition('uid', $current_uid)
    ->condition('lmsbook', $lmsbook);

    $ids = $query->execute();
    if(empty($ids)) {
      $create_request = RequestedLmsBook::create([
        'title'        => 'Request for the book',
        'uid'       => $current_uid,
        'lmsbook'       => $lmsbook
      ]);
      $create_request->save();
      $type = 'status';
      $message = t('Successfully created a book request');
    } else {
      $type = 'error';
      $message = t('Book already requested');
    }

    \Drupal::messenger()->addMessage($message, $type);

    $route_name = 'entity.lmsbook.canonical';
    return $this->redirect($route_name, ['lmsbook' => $lmsbook]);
  }

}
