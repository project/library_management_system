<?php

namespace Drupal\library_management_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\library_management_system\LmsBookAuthorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\library_management_system\Entity\LmsBookAuthor;
use Drupal\library_management_system\Entity\LmsBook;
use Drupal\news_article\Entity\NewsArticle;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns responses for lmsbookauthor module routes.
 */
class LmsBookAuthorController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\library_management_system\LmsBookAuthorInterface $lmsbookauthor
   *   The lmsbookauthor entity.
   *
   * @return string
   *   The lmsbookauthor label.
   */
  public function lmsbookauthorTitle(LmsBookAuthor $lmsbookauthor) {
    return $lmsbookauthor->label();
  }
}
