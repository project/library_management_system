<?php

namespace Drupal\library_management_system\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An library_management_system controller.
 */
class ReportController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function book_reports() {
  	return [
  		'#theme' => 'book_reports_template',
  		'#data' => $this->t('Reports'),
  	];
  }

}