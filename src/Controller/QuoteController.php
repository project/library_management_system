<?php

namespace Drupal\library_management_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\library_management_system\QuoteInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\library_management_system\Entity\Quote;
use Drupal\library_management_system\Entity\Author;
use Drupal\library_management_system\Entity\Tag;
use Drupal\news_article\Entity\NewsArticle;
use Drupal\taxonomy\Entity\Term;
/**
 * Returns responses for quote module routes.
 */
class QuoteController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\library_management_system\QuoteInterface $quote
   *   The quote entity.
   *
   * @return string
   *   The quote label.
   */
  public function quoteTitle(Quote $quote) {
    return $quote->label();
  }
}
