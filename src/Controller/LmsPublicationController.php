<?php

namespace Drupal\library_management_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\library_management_system\LmsPublicationInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\library_management_system\Entity\LmsPublication;
use Drupal\library_management_system\Entity\LmsBook;
use Drupal\news_article\Entity\NewsArticle;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns responses for lmspublication module routes.
 */
class LmsPublicationController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\library_management_system\LmsPublicationInterface $lmspublication
   *   The lmspublication entity.
   *
   * @return string
   *   The lmspublication label.
   */
  public function lmspublicationTitle(LmsPublication $lmspublication) {
    return $lmspublication->label();
  }
}
