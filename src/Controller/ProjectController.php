<?php

namespace Drupal\library_management_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\library_management_system\LmsBookInterface;
use Drupal\library_management_system\Entity\LmsBook;

/**
 * Returns responses for lmsbook module routes.
 */
class LmsBookController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\library_management_system\LmsBookInterface $lmsbook
   *   The lmsbook entity.
   *
   * @return string
   *   The lmsbook label.
   */
  public function lmsbookTitle(LmsBook $lmsbook) {
    return $lmsbook->label();
  }

}
