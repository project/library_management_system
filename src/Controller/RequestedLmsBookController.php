<?php

namespace Drupal\library_management_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\library_management_system\RequestedLmsBookInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\library_management_system\Entity\LmsBookAuthor;
use Drupal\library_management_system\Entity\RequestedLmsBook;
use Drupal\news_article\Entity\NewsArticle;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns responses for requestedlmsbook module routes.
 */
class RequestedLmsBookController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\library_management_system\RequestedLmsBookInterface $requestedlmsbook
   *   The requestedlmsbook entity.
   *
   * @return string
   *   The requestedlmsbook label.
   */
  public function requestedlmsbookTitle(RequestedLmsBook $requestedlmsbook) {
    return $requestedlmsbook->label();
  }

  /**
   * Issue book
   * @param  RequestedLmsBook $requestedlmsbook [description]
   * @return [type]                             [description]
   */
  public function issueBook(RequestedLmsBook $requestedlmsbook) {
    $status = true;
    $requestedlmsbook->setStatus($status);
    $requestedlmsbook->save();

    $entity_title = $requestedlmsbook->getTitle();
    $message = '"'.$entity_title.'" has been issued successfully.';

    \Drupal::messenger()->addStatus(t($message));

    return $this->redirect('requestedlmsbook.settings');
  }
}
