<?php

namespace Drupal\library_management_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\library_management_system\IssuedLmsBookInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\library_management_system\Entity\IssuedLmsBook;
use Drupal\library_management_system\Entity\LmsBook;
use Drupal\news_article\Entity\NewsArticle;
use Drupal\taxonomy\Entity\Term;

/**
 * Returns responses for issuedlmsbook module routes.
 */
class IssuedLmsBookController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\library_management_system\IssuedLmsBookInterface $issuedlmsbook
   *   The issuedlmsbook entity.
   *
   * @return string
   *   The issuedlmsbook label.
   */
  public function issuedlmsbookTitle(IssuedLmsBook $issuedlmsbook) {
    return $issuedlmsbook->label();
  }
}
