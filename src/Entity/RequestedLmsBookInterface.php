<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining RequestedLmsBook entities.
 *
 * @ingroup library_management_system
 */
interface RequestedLmsBookInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the RequestedLmsBook title.
   *
   * @return string
   *   Title of the RequestedLmsBook.
   */
  public function getTitle();

  /**
   * Sets the RequestedLmsBook title.
   *
   * @param string $title
   *   The RequestedLmsBook title.
   *
   * @return \Drupal\library_management_system\Entity\RequestedLmsBookInterface
   *   The called RequestedLmsBook entity.
   */
  public function setTitle($title);

  /**
   * Gets the RequestedLmsBook creation timestamp.
   *
   * @return int
   *   Creation timestamp of the RequestedLmsBook.
   */
  public function getCreatedTime();

  /**
   * Sets the RequestedLmsBook creation timestamp.
   *
   * @param int $timestamp
   *   The RequestedLmsBook creation timestamp.
   *
   * @return \Drupal\library_management_system\Entity\RequestedLmsBookInterface
   *   The called RequestedLmsBook entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the RequestedLmsBook published status indicator.
   *
   * Unpublished RequestedLmsBook are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the RequestedLmsBook is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a RequestedLmsBook.
   *
   * @param bool $published
   *   TRUE to set this RequestedLmsBook to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\library_management_system\Entity\RequestedLmsBookInterface
   *   The called RequestedLmsBook entity.
   */
  public function setPublished($published);

}
