<?php

namespace Drupal\library_management_system\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for LmsBookAuthor entities.
 */
class LmsBookAuthorViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
