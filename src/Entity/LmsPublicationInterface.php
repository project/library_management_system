<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining LmsPublication entities.
 *
 * @ingroup library_management_system
 */
interface LmsPublicationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the LmsPublication title.
   *
   * @return string
   *   Title of the LmsPublication.
   */
  public function getTitle();

  /**
   * Sets the LmsPublication title.
   *
   * @param string $title
   *   The LmsPublication title.
   *
   * @return \Drupal\library_management_system\Entity\LmsPublicationInterface
   *   The called LmsPublication entity.
   */
  public function setTitle($title);

  /**
   * Gets the LmsPublication creation timestamp.
   *
   * @return int
   *   Creation timestamp of the LmsPublication.
   */
  public function getCreatedTime();

  /**
   * Sets the LmsPublication creation timestamp.
   *
   * @param int $timestamp
   *   The LmsPublication creation timestamp.
   *
   * @return \Drupal\library_management_system\Entity\LmsPublicationInterface
   *   The called LmsPublication entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the LmsPublication published status indicator.
   *
   * Unpublished LmsPublication are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the LmsPublication is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a LmsPublication.
   *
   * @param bool $published
   *   TRUE to set this LmsPublication to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\library_management_system\Entity\LmsPublicationInterface
   *   The called LmsPublication entity.
   */
  public function setPublished($published);

}
