<?php

namespace Drupal\library_management_system\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for LmsBook entities.
 */
class LmsBookViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
