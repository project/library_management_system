<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining LmsBook entities.
 *
 * @ingroup library_management_system
 */
interface LmsBookInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the LmsBook title.
   *
   * @return string
   *   Title of the LmsBook.
   */
  public function getTitle();

  /**
   * Sets the LmsBook title.
   *
   * @param string $title
   *   The LmsBook title.
   *
   * @return \Drupal\library_management_system\Entity\LmsBookInterface
   *   The called LmsBook entity.
   */
  public function setTitle($title);

  /**
   * Gets the LmsBook creation timestamp.
   *
   * @return int
   *   Creation timestamp of the LmsBook.
   */
  public function getCreatedTime();

  /**
   * Sets the LmsBook creation timestamp.
   *
   * @param int $timestamp
   *   The LmsBook creation timestamp.
   *
   * @return \Drupal\library_management_system\Entity\LmsBookInterface
   *   The called LmsBook entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the LmsBook published status indicator.
   *
   * Unpublished LmsBook are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the LmsBook is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a LmsBook.
   *
   * @param bool $published
   *   TRUE to set this LmsBook to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\library_management_system\Entity\LmsBookInterface
   *   The called LmsBook entity.
   */
  public function setPublished($published);

}
