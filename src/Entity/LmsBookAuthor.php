<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the LmsBookAuthor entity.
 *
 * @ingroup library_management_system
 *
 * @ContentEntityType(
 *   id = "lmsbookauthor",
 *   label = @Translation("LmsBookAuthor"),
 *   handlers = {
 *     "view_builder" = "Drupal\library_management_system\LmsBookAuthorViewBuilder",
 *     "list_builder" = "Drupal\library_management_system\LmsBookAuthorListBuilder",
 *     "views_data" = "Drupal\library_management_system\Entity\LmsBookAuthorViewsData",
 *     "translation" = "Drupal\library_management_system\LmsBookAuthorTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\library_management_system\Form\LmsBookAuthorForm",
 *       "add" = "Drupal\library_management_system\Form\LmsBookAuthorForm",
 *       "edit" = "Drupal\library_management_system\Form\LmsBookAuthorForm",
 *       "delete" = "Drupal\library_management_system\Form\LmsBookAuthorDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\library_management_system\Form\ConfirmDeleteMultipleLmsBookAuthor",
 *     },
 *     "access" = "Drupal\library_management_system\LmsBookAuthorAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\library_management_system\LmsBookAuthorHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "lmsbookauthor",
 *   data_table = "lmsbookauthor",
 *   translatable = FALSE,
 *   admin_permission = "administer lmsbookauthor entities",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/lmsbookauthor/{lmsbookauthor}",
 *     "add-form" = "/admin/structure/lmsbookauthor/add",
 *     "edit-form" = "/admin/structure/lmsbookauthor/{lmsbookauthor}/edit",
 *     "delete-form" = "/admin/structure/lmsbookauthor/{lmsbookauthor}/delete",
 *   },
 *   field_ui_base_route = "lmsbookauthor.settings"
 * )
 */
class LmsBookAuthor extends ContentEntityBase implements LmsBookAuthorInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    try {
      return $this->get('title')->value;
    } catch (Exception $e) {
      return '';
    }
  }

   /**
   * {@inheritdoc}
   */
   public function getTitleLink() {

    $title = $this->get('title')->value;
    $link = $this->toUrl()->toString();
    $markup = "<a href='".$link."'>".$title."</a>";

    return ['#markup' => $markup];

  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($created) {
    $this->set('created', $created);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }


  /**
   * {@inheritdoc}
   */
  public function isOpen() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUrl() {
    $path = '';
    if($this->get('image')) {
      $uri = $this->get('image')->entity->getFileUri();
      $path = \Drupal\image\Entity\ImageStyle::load('medium')->buildUrl($uri);
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed() {
    return (bool) $this->get('status')->value == 0;
  }

  /**
   * {@inheritdoc}
   */
  public function close() {
    return $this->set('status', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function open() {
    return $this->set('status', 1);
  }

    /**
   * {@inheritdoc}
   */
    public function isPublished() {
      return (bool) $this->getEntityKey('status');
    }
  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }


    /**
   * {@inheritdoc}
   */
    public function setChangedTime($published) {
    // $this->set('status', $published ? TRUE : FALSE);
      return $this;
    }
  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    return $this->get('tags')->value;
  }
  /**
   * {@inheritdoc}
   */
  public function getRequestedLmsBookAuthor() {

    return file_create_url($this->get('requestedlmsbookauthor')->entity->image->entity->getFileUri());
  }

      /**
   * Generate array to export data
   * @return
   */
      public function getExcelData() {
        $data['Title'] = $this->getTitle();
        $CreatedTime = $this->getCreatedTime();
        // $data['Owner'] = $this->getOwner()->get('name')->value;
        // $data['OwnerId'] = $this->getOwnerId();
        // $data['Status'] = $this->getStatus();
        $ChangedTime = $this->getChangedTime();

        $ChangedTime = \Drupal::service('date.formatter')->format($ChangedTime, 'custom', 'M d Y');
        $CreatedTime = \Drupal::service('date.formatter')->format($CreatedTime, 'custom', 'M d Y');

        $data['ChangedTime'] = $ChangedTime;
        $data['CreatedTime'] = $CreatedTime;

        return $data;
      }

  /**
   * {@inheritdoc}
   */
  public function getCategoryForIsotope() {
    $title = '';
    if($this->get('lmsbookauthor_category')) {
      $lmsbookauthor_category = $this->get('lmsbookauthor_category')->entity;
      $title = $lmsbookauthor_category->name->value;
      $title = strtolower(str_replace(' ', '-', $title));
    }

    return $title;
  }

    /**
   * {@inheritdoc}
   */
    public function getOwnerName() {
      $author = $this->get('uid')->entity->name->value;
      $author = ($author != '')?$author:'Anonymous';
      return $author;
    }
  /**
   * {@inheritdoc}
   */
  public function getRelatedLmsBookAuthors() {
    return $this->get('related_lmsbookauthors')->value;
  }
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('LmsBookAuthor ID'))
    ->setDescription(t('The ID of the lmsbookauthor.'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
    ->setLabel(t('UUID'))
    ->setDescription(t('The lmsbookauthor UUID.'))
    ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Title'))
    ->setDescription(t('The lmsbookauthor title.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', array(
      'type' => 'string_textfield',
      'weight' => 1,
    ))
    ->setDisplayConfigurable('form', TRUE);

    $fields['text_long'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Content'))
    ->setDescription(t('Main content of the lmsbookauthor.'))
    ->setDisplayOptions('view', [
      'label' => 'visible',
      'type' => 'text_default',
      'weight' => 2,
    ])
    ->setDisplayOptions('form', [
      'type' => 'text_textarea',
      'weight' => 2,
      'rows' => 6,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
    ->setLabel(t('Language code'))
    ->setDescription(t('The lmsbookauthor language code.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('When the lmsbookauthor was created, as a Unix timestamp.'));

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return array(\Drupal::currentUser()->id());
  }

  /**
   *
   * {@inheritdoc}
   */
  public static function sort($a, $b) {
    return strcmp($a->label(), $b->label());
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // codes here
  }
}
