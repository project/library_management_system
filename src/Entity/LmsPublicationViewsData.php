<?php

namespace Drupal\library_management_system\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for LmsPublication entities.
 */
class LmsPublicationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
  	$data = parent::getViewsData();

  	return $data;
  }

}
