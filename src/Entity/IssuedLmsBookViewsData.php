<?php

namespace Drupal\library_management_system\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for IssuedLmsBook entities.
 */
class IssuedLmsBookViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
