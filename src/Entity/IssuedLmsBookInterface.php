<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining IssuedLmsBook entities.
 *
 * @ingroup library_management_system
 */
interface IssuedLmsBookInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the IssuedLmsBook title.
   *
   * @return string
   *   Title of the IssuedLmsBook.
   */
  public function getTitle();

  /**
   * Sets the IssuedLmsBook title.
   *
   * @param string $title
   *   The IssuedLmsBook title.
   *
   * @return \Drupal\library_management_system\Entity\IssuedLmsBookInterface
   *   The called IssuedLmsBook entity.
   */
  public function setTitle($title);

  /**
   * Gets the IssuedLmsBook creation timestamp.
   *
   * @return int
   *   Creation timestamp of the IssuedLmsBook.
   */
  public function getCreatedTime();

  /**
   * Sets the IssuedLmsBook creation timestamp.
   *
   * @param int $timestamp
   *   The IssuedLmsBook creation timestamp.
   *
   * @return \Drupal\library_management_system\Entity\IssuedLmsBookInterface
   *   The called IssuedLmsBook entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the IssuedLmsBook published status indicator.
   *
   * Unpublished IssuedLmsBook are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the IssuedLmsBook is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a IssuedLmsBook.
   *
   * @param bool $published
   *   TRUE to set this IssuedLmsBook to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\library_management_system\Entity\IssuedLmsBookInterface
   *   The called IssuedLmsBook entity.
   */
  public function setPublished($published);

}
