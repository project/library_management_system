<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the IssuedLmsBook entity.
 *
 * @ingroup library_management_system
 *
 * @ContentEntityType(
 *   id = "issuedlmsbook",
 *   label = @Translation("Issued Book"),
 *   handlers = {
 *     "view_builder" = "Drupal\library_management_system\IssuedLmsBookViewBuilder",
 *     "list_builder" = "Drupal\library_management_system\IssuedLmsBookListBuilder",
 *     "views_data" = "Drupal\library_management_system\Entity\IssuedLmsBookViewsData",
 *     "translation" = "Drupal\library_management_system\IssuedLmsBookTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\library_management_system\Form\IssuedLmsBookForm",
 *       "add" = "Drupal\library_management_system\Form\IssuedLmsBookForm",
 *       "edit" = "Drupal\library_management_system\Form\IssuedLmsBookForm",
 *       "delete" = "Drupal\library_management_system\Form\IssuedLmsBookDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\library_management_system\Form\ConfirmDeleteMultipleIssuedLmsBook",
 *     },
 *     "access" = "Drupal\library_management_system\IssuedLmsBookAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\library_management_system\IssuedLmsBookHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "issuedlmsbook",
 *   data_table = "issuedlmsbook",
 *   translatable = FALSE,
 *   admin_permission = "administer issuedlmsbook entities",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/issuedlmsbook/{issuedlmsbook}",
 *     "add-form" = "/admin/structure/issuedlmsbook/add",
 *     "edit-form" = "/admin/structure/issuedlmsbook/{issuedlmsbook}/edit",
 *     "delete-form" = "/admin/structure/issuedlmsbook/{issuedlmsbook}/delete",
 *   },
 *   field_ui_base_route = "issuedlmsbook.settings"
 * )
 */
class IssuedLmsBook extends ContentEntityBase implements IssuedLmsBookInterface {

  use EntityChangedTrait;



  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setFinePrice($fine_price) {
    $this->set('fine_price', $fine_price);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($created) {
    $this->set('created', $created);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getReturnedTime() {
    if($this->get('returned_date')->value) {
      $returned_date = $this->get('returned_date')->value;
      return strtotime($returned_date);
    } else {
      return '';
    }
  }


  /**
   * {@inheritdoc}
   */
  public function isOpen() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUrl() {
    $path = '';
    if($this->get('image')) {
      $uri = $this->get('image')->entity->getFileUri();
      $path = \Drupal\image\Entity\ImageStyle::load('medium')->buildUrl($uri);
    }
    return $path;
  }
  /**
   * {@inheritdoc}
   */
  public function getIssuedLmsBookUrl() {
    $path = $this->toUrl()->toString();
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed() {
    return (bool) $this->get('status')->value == 0;
  }

  /**
   * {@inheritdoc}
   */
  public function close() {
    return $this->set('status', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function open() {
    return $this->set('status', 1);
  }

    /**
   * {@inheritdoc}
   */
    public function isPublished() {
      return (bool) $this->getEntityKey('status');
    }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

    /**
   * {@inheritdoc}
   */
    public function setChangedTime($published) {
    // $this->set('status', $published ? TRUE : FALSE);
      return $this;
    }
  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    return $this->get('tags')->value;
  }

    /**
   * Generate array to export data
   * @return
   */
    public function getExcelData() {
      $data['Title'] = $this->getTitle();
      $CreatedTime = $this->getCreatedTime();
      $data['Owner'] = $this->getOwner()->get('name')->value;
      $data['OwnerId'] = $this->getOwnerId();
      $data['Status'] = $this->getStatus();
      $ChangedTime = $this->getChangedTime();

      $ChangedTime = \Drupal::service('date.formatter')->format($ChangedTime, 'custom', 'M d Y');
      $CreatedTime = \Drupal::service('date.formatter')->format($CreatedTime, 'custom', 'M d Y');

      $data['ChangedTime'] = $ChangedTime;
      $data['CreatedTime'] = $CreatedTime;

      return $data;
    }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Issued Book ID'))
    ->setDescription(t('The ID of the issued book.'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
    ->setLabel(t('UUID'))
    ->setDescription(t('The issued book UUID.'))
    ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Title'))
    ->setDescription(t('The issued book title.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', array(
      'type' => 'string_textfield',
      'weight' => 1,
    ))
    ->setDisplayConfigurable('form', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Student'))
    ->setDescription(t('The book student.'))
    ->setSetting('target_type', 'user')
    ->setTranslatable(TRUE)
    ->setDisplayOptions('form', array(
      'type' => 'entity_reference_autocomplete',
      'weight' => 2,
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'autocomplete_type' => 'tags',
        'placeholder' => '',
      ),
    ))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 2,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['lmsbook'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Book'))
    ->setDescription(t('The book.'))
    ->setSetting('target_type', 'lmsbook')
    ->setTranslatable(TRUE)
    ->setDisplayOptions('form', array(
      'type' => 'entity_reference_autocomplete',
      'weight' => 3,
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'autocomplete_type' => 'tags',
        'placeholder' => '',
      ),
    ))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 3,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);


    $fields['due_date'] = BaseFieldDefinition::create('datetime')
    ->setLabel(t('Due date'))
    ->setDescription(t('Due date'))
    ->setRevisionable(TRUE)
    ->setDefaultValue('')
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 4,
    ])
    ->setDisplayOptions('form', [
      'type' => 'datetime_default',
      'weight' => 4,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['returned_date'] = BaseFieldDefinition::create('datetime')
    ->setLabel(t('Returned date'))
    ->setDescription(t('Returned date'))
    ->setRevisionable(TRUE)
    ->setDefaultValue('')
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'datetime_default',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 5,
    ])
    ->setDisplayOptions('form', [
      'type' => 'datetime_default',
      'weight' => 5,
      'default_value' => DrupalDateTime::createFromTimestamp(time()),
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['fine_price'] = BaseFieldDefinition::create('decimal')
    ->setLabel(t('Fine Price Per Day'))
    ->setDescription(t('Fine price'))
    ->setSettings(array(
      'precision' => 5,
      'scale' => 2,
    ))
    ->setDisplayOptions('view', array(
      'label' => 'above',
      'type' => 'number_decimal',
      'weight' => 6,
    ))
    ->setDisplayOptions('form', array(
      'type' => 'number',
      'weight' => 6,
    ))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['notes'] = BaseFieldDefinition::create('string_long')
    ->setLabel(t('Notes'))
    ->setDescription(t('Short notes of the issued.'))
    ->setDefaultValue('')
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ->setRequired(FALSE)
    ->setDisplayOptions('view', [
      'label' => 'visible',
      'type' => 'basic_string',
      'weight' => 7,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textarea',
      'weight' => 7,
      'settings' => ['rows' => 4],
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);


    $fields['status'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Book returned status'))
    ->setDescription(t('The status of the book return.'))
    ->setReadOnly(TRUE)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 8,
    ])
    ->setDisplayConfigurable('view', TRUE);

    $fields['request_id'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Book request id'))
    ->setDescription(t('The request id of the book.'))
    ->setDefaultValue(0)
    ->setReadOnly(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
    ->setLabel(t('Language code'))
    ->setDescription(t('The issued book language code.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('When the issuedlmsbook was created, as a Unix timestamp.'));

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return array(\Drupal::currentUser()->id());
  }

  /**
   *
   * {@inheritdoc}
   */
  public static function sort($a, $b) {
    return strcmp($a->label(), $b->label());
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // codes here
  }
}
