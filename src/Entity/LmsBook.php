<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the LmsBook entity.
 *
 * @ingroup library_management_system
 *
 * @ContentEntityType(
 *   id = "lmsbook",
 *   label = @Translation("Book"),
 *   handlers = {
 *     "view_builder" = "Drupal\library_management_system\LmsBookViewBuilder",
 *     "list_builder" = "Drupal\library_management_system\LmsBookListBuilder",
 *     "views_data" = "Drupal\library_management_system\Entity\LmsBookViewsData",
 *     "translation" = "Drupal\library_management_system\LmsBookTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\library_management_system\Form\LmsBookForm",
 *       "add" = "Drupal\library_management_system\Form\LmsBookForm",
 *       "edit" = "Drupal\library_management_system\Form\LmsBookForm",
 *       "delete" = "Drupal\library_management_system\Form\LmsBookDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\library_management_system\Form\ConfirmDeleteMultipleLmsBook",
 *     },
 *     "access" = "Drupal\library_management_system\LmsBookAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\library_management_system\LmsBookHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "lmsbook",
 *   data_table = "lmsbook",
 *   translatable = FALSE,
 *   admin_permission = "administer lmsbook entities",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/lmsbook/{lmsbook}",
 *     "add-form" = "/admin/structure/lmsbook/add",
 *     "edit-form" = "/admin/structure/lmsbook/{lmsbook}/edit",
 *     "delete-form" = "/admin/structure/lmsbook/{lmsbook}/delete",
 *   },
 *   field_ui_base_route = "lmsbook.settings"
 * )
 */
class LmsBook extends ContentEntityBase implements LmsBookInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

   /**
   * {@inheritdoc}
   */
   public function getTitleLink() {

    $title = $this->get('title')->value;
    $link = $this->toUrl()->toString();
    $markup = "<a href='".$link."'>".$title."</a>";

    return ['#markup' => $markup];

  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($created) {
    $this->set('created', $created);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }


  /**
   * {@inheritdoc}
   */
  public function isOpen() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUrl() {
    $path = '';
    if($this->get('image')) {
      $uri = $this->get('image')->entity->getFileUri();
      $path = \Drupal\image\Entity\ImageStyle::load('medium')->buildUrl($uri);
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed() {
    return (bool) $this->get('status')->value == 0;
  }

  /**
   * {@inheritdoc}
   */
  public function close() {
    return $this->set('status', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function open() {
    return $this->set('status', 1);
  }

    /**
   * {@inheritdoc}
   */
    public function isPublished() {
      return (bool) $this->getEntityKey('status');
    }
  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }


    /**
   * {@inheritdoc}
   */
    public function setChangedTime($published) {
    // $this->set('status', $published ? TRUE : FALSE);
      return $this;
    }
  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    return $this->get('tags')->value;
  }
  /**
   * {@inheritdoc}
   */
  public function getNoOfCopies() {
    return $this->get('copies')->value;
  }
  /**
   * {@inheritdoc}
   */
  public function getRequestedLmsBook() {

    return file_create_url($this->get('requestedlmsbook')->entity->image->entity->getFileUri());
  }

    /**
   * Generate array to export data
   * @return
   */
    function getExcelData() {
      $data['Title'] = $this->getTitle();
      $CreatedTime = $this->getCreatedTime();
      $data['Owner'] = $this->getOwner()->get('name')->value;
      $data['OwnerId'] = $this->getOwnerId();
      $ChangedTime = $this->getChangedTime();

      $ChangedTime = \Drupal::service('date.formatter')->format($ChangedTime, 'custom', 'M d Y');
      $CreatedTime = \Drupal::service('date.formatter')->format($CreatedTime, 'custom', 'M d Y');

      $data['ChangedTime'] = $ChangedTime;
      $data['CreatedTime'] = $CreatedTime;

      return $data;
    }

  /**
   * {@inheritdoc}
   */
  public function getCategoryForIsotope() {
    $title = '';
    if($this->get('lmsbook_category')) {
      $lmsbook_category = $this->get('lmsbook_category')->entity;
      $title = $lmsbook_category->name->value;
      $title = strtolower(str_replace(' ', '-', $title));
    }

    return $title;
  }

    /**
   * {@inheritdoc}
   */
    public function getOwnerName() {
      $author = $this->get('uid')->entity->name->value;
      $author = ($author != '')?$author:'Anonymous';
      return $author;
    }
  /**
   * {@inheritdoc}
   */
  public function getRelatedLmsBooks() {
    return $this->get('related_lmsbooks')->value;
  }
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('LmsBook ID'))
    ->setDescription(t('The ID of the lmsbook.'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
    ->setLabel(t('UUID'))
    ->setDescription(t('The lmsbook UUID.'))
    ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Title'))
    ->setDescription(t('The lmsbook title.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', array(
      'type' => 'string_textfield',
      'weight' => 1,
    ))
    ->setDisplayConfigurable('form', TRUE);


    $fields['featured_image'] = BaseFieldDefinition::create('image')
    ->setLabel(t('Featured Image'))
    ->setDescription(t('Image field'))
    ->setSettings([
      'file_directory' => 'lms/[date:custom:Y]-[date:custom:m]',
      'alt_field_required' => FALSE,
      'file_extensions' => 'png jpg jpeg',
    ])
    ->setDisplayOptions('view', array(
      'label' => 'hidden',
      'type' => 'default',
      'weight' => 2,
    ))
    ->setDisplayOptions('form', array(
      'label' => 'hidden',
      'type' => 'image_image',
      'weight' => 2,
    ))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['details'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Book Details'))
    ->setDescription(t('Main content of the lmsbook.'))
    ->setDisplayOptions('view', [
      'label' => 'visible',
      'type' => 'text_default',
      'weight' => 3,
    ])
    ->setDisplayOptions('form', [
      'type' => 'text_textarea',
      'weight' => 3,
      'rows' => 6,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);


    $fields['lmsbook_category'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Category'))
    ->setDescription(t('The lmsbook category.'))
    ->setSetting('target_type', 'taxonomy_term')
    ->setSetting('handler_settings',
      [
        'target_bundles' =>
        [
          'taxonomy_term' => 'lmsbook_categories'
        ]
      ]
    )
    ->setDisplayOptions('view', array(
      'label' => 'above',
      'type' => 'taxonomy_term',
    ))
    ->setDefaultValue(NULL)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'size' => 60,
        'placeholder' => '',
      ],
      'weight' => 4,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Author'))
    ->setDescription(t('The lmsbook author.'))
    ->setSetting('target_type', 'lmsbookauthor')
    ->setTranslatable(TRUE)
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ->setDisplayOptions('form', array(
      'type' => 'entity_reference_autocomplete',
      'weight' => 5,
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'autocomplete_type' => 'tags',
        'placeholder' => '',
      ),
    ))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 5,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['lmspublication'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Book Publication'))
    ->setDescription(t('The book publication.'))
    ->setSetting('target_type', 'lmspublication')
    ->setTranslatable(TRUE)
    ->setDisplayOptions('form', array(
      'type' => 'entity_reference_autocomplete',
      'weight' => 6,
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'autocomplete_type' => 'tags',
        'placeholder' => '',
      ),
    ))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 6,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['isbn'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Isbn Number'))
    ->setDescription(t('The book Isbn Number.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', array(
      'type' => 'string_textfield',
      'weight' => 7,
    ))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 7,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);


    $fields['copies'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('No of copies'))
    ->setDescription(t('No of available copies'))
    ->setDisplayOptions('view', array(
      'label' => 'above',
      'type' => 'number_decimal',
      'weight' => 8,
    ))
    ->setDisplayOptions('form', array(
      'type' => 'number',
      'weight' => 8,
    ))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['price'] = BaseFieldDefinition::create('decimal')
    ->setLabel(t('Price'))
    ->setDescription(t('LmsBook price'))
    ->setSettings(array(
      'precision' => 5,
      'scale' => 2,
    ))
    ->setDisplayOptions('view', array(
      'label' => 'above',
      'type' => 'number_decimal',
      'weight' => 9,
    ))
    ->setDisplayOptions('form', array(
      'type' => 'number',
      'weight' => 9,
    ))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
    ->setLabel(t('Language code'))
    ->setDescription(t('The lmsbook language code.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('When the lmsbook was created, as a Unix timestamp.'));

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return array(\Drupal::currentUser()->id());
  }

  /**
   *
   * {@inheritdoc}
   */
  public static function sort($a, $b) {
    return strcmp($a->label(), $b->label());
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // codes here
  }
}
