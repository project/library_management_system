<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the RequestedLmsBook entity.
 *
 * @ingroup library_management_system
 *
 * @ContentEntityType(
 *   id = "requestedlmsbook",
 *   label = @Translation("Requested Book"),
 *   handlers = {
 *     "view_builder" = "Drupal\library_management_system\RequestedLmsBookViewBuilder",
 *     "list_builder" = "Drupal\library_management_system\RequestedLmsBookListBuilder",
 *     "views_data" = "Drupal\library_management_system\Entity\RequestedLmsBookViewsData",
 *     "translation" = "Drupal\library_management_system\RequestedLmsBookTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\library_management_system\Form\RequestedLmsBookForm",
 *       "add" = "Drupal\library_management_system\Form\RequestedLmsBookForm",
 *       "edit" = "Drupal\library_management_system\Form\RequestedLmsBookForm",
 *       "delete" = "Drupal\library_management_system\Form\RequestedLmsBookDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\library_management_system\Form\ConfirmDeleteMultipleRequestedLmsBook",
 *     },
 *     "access" = "Drupal\library_management_system\RequestedLmsBookAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\library_management_system\RequestedLmsBookHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "requestedlmsbook",
 *   data_table = "requestedlmsbook",
 *   translatable = FALSE,
 *   admin_permission = "administer requestedlmsbook entities",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/requestedlmsbook/{requestedlmsbook}",
 *     "add-form" = "/admin/structure/requestedlmsbook/add",
 *     "edit-form" = "/admin/structure/requestedlmsbook/{requestedlmsbook}/edit",
 *     "delete-form" = "/admin/structure/requestedlmsbook/{requestedlmsbook}/delete",
 *   },
 *   field_ui_base_route = "requestedlmsbook.settings"
 * )
 */
class RequestedLmsBook extends ContentEntityBase implements RequestedLmsBookInterface {

  use EntityChangedTrait;



  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($created) {
    $this->set('created', $created);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookId() {
    return $this->get('lmsbook')->value;
  }


  /**
   * {@inheritdoc}
   */
  public function isOpen() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestedLmsBookUrl() {
    $path = $this->toUrl()->toString();
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed() {
    return (bool) $this->get('status')->value == 0;
  }

  /**
   * {@inheritdoc}
   */
  public function close() {
    return $this->set('status', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function open() {
    return $this->set('status', 1);
  }

    /**
   * {@inheritdoc}
   */
    public function isPublished() {
      return (bool) $this->getEntityKey('status');
    }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

    /**
   * {@inheritdoc}
   */
    public function getStatus() {
      return $this->get('status')->value;
    }

    /**
   * {@inheritdoc}
   */
    public function setChangedTime($published) {
    // $this->set('status', $published ? TRUE : FALSE);
      return $this;
    }
  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('created')->value;
  }


  /**
   * Generate array to export data
   * @return
   */
  public function getExcelData() {
    $data['Title'] = $this->getTitle();
    $CreatedTime = $this->getCreatedTime();
    $data['BookId'] = $this->getBookId();
    $data['Owner'] = $this->getOwner()->get('name')->value;
    $data['OwnerId'] = $this->getOwnerId();
    $data['RequestedLmsBookUrl'] = $this->getRequestedLmsBookUrl();
    $data['Status'] = $this->getStatus();
    $ChangedTime = $this->getChangedTime();

    $ChangedTime = \Drupal::service('date.formatter')->format($ChangedTime, 'custom', 'M d Y');
    $CreatedTime = \Drupal::service('date.formatter')->format($CreatedTime, 'custom', 'M d Y');

    $data['ChangedTime'] = $ChangedTime;
    $data['CreatedTime'] = $CreatedTime;

    return $data;
  }
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Requested Book ID'))
    ->setDescription(t('The ID of the requestedlmsbook.'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
    ->setLabel(t('UUID'))
    ->setDescription(t('The requested book UUID.'))
    ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Title'))
    ->setDescription(t('The requested book title.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', array(
      'type' => 'string_textfield',
      'weight' => 1,
    ))
    ->setDisplayConfigurable('form', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Student'))
    ->setDescription(t('The book student.'))
    ->setSetting('target_type', 'user')
    ->setTranslatable(TRUE)
    ->setDisplayOptions('form', array(
      'type' => 'entity_reference_autocomplete',
      'weight' => 2,
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'autocomplete_type' => 'tags',
        'placeholder' => '',
      ),
    ))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 2,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['lmsbook'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Book'))
    ->setDescription(t('The book.'))
    ->setSetting('target_type', 'lmsbook')
    ->setTranslatable(TRUE)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 3,
    ])
    ->setDisplayOptions('form', array(
      'type' => 'entity_reference_autocomplete',
      'weight' => 3,
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'autocomplete_type' => 'tags',
        'placeholder' => '',
      ),
    ))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Issued'))
    ->setDescription(t('A flag indicating whether the requested book is issued.'))
    ->setDefaultValue(0)
    ->setDisplayOptions('form', array(
      'type' => 'boolean_checkbox',
      'settings' => array(
        'display_label' => TRUE,
      ),
      'weight' => 4,
    ))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'settings' => [
        'format_type' => 'medium',
      ],
      'weight' => 4,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
    ->setLabel(t('Language code'))
    ->setDescription(t('The requested book language code.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('When the requested book was created, as a Unix timestamp.'));

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return array(\Drupal::currentUser()->id());
  }

  /**
   *
   * {@inheritdoc}
   */
  public static function sort($a, $b) {
    return strcmp($a->label(), $b->label());
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // codes here
  }
}
