<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the LmsPublication entity.
 *
 * @ingroup library_management_system
 *
 * @ContentEntityType(
 *   id = "lmspublication",
 *   label = @Translation("Publication"),
 *   handlers = {
 *     "view_builder" = "Drupal\library_management_system\LmsPublicationViewBuilder",
 *     "list_builder" = "Drupal\library_management_system\LmsPublicationListBuilder",
 *     "views_data" = "Drupal\library_management_system\Entity\LmsPublicationViewsData",
 *     "translation" = "Drupal\library_management_system\LmsPublicationTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\library_management_system\Form\LmsPublicationForm",
 *       "add" = "Drupal\library_management_system\Form\LmsPublicationForm",
 *       "edit" = "Drupal\library_management_system\Form\LmsPublicationForm",
 *       "delete" = "Drupal\library_management_system\Form\LmsPublicationDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\node\Form\DeleteMultiple"
 *     },
 *     "access" = "Drupal\library_management_system\LmsPublicationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\library_management_system\LmsPublicationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "lmspublication",
 *   data_table = "lmspublication",
 *   translatable = FALSE,
 *   admin_permission = "administer lmspublication entities",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/lmspublication/{lmspublication}",
 *     "add-form" = "/admin/structure/lmspublication/add",
 *     "edit-form" = "/admin/structure/lmspublication/{lmspublication}/edit",
 *     "delete-form" = "/admin/structure/lmspublication/{lmspublication}/delete",
 *     "delete-multiple-form" = "/admin/structure/lmspublication/delete",
 *   },
 *   field_ui_base_route = "lmspublication.settings"
 * )
 */
class LmsPublication extends ContentEntityBase implements LmsPublicationInterface {

  use EntityChangedTrait;


  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($created) {
    $this->set('created', $created);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }


  /**
   * {@inheritdoc}
   */
  public function isOpen() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUrl() {
    $path = '';
    if($this->get('image')) {
      $uri = $this->get('image')->entity->getFileUri();
      $path = \Drupal\image\Entity\ImageStyle::load('medium')->buildUrl($uri);
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed() {
    return (bool) $this->get('status')->value == 0;
  }

  /**
   * {@inheritdoc}
   */
  public function close() {
    return $this->set('status', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function open() {
    return $this->set('status', 1);
  }

    /**
   * {@inheritdoc}
   */
    public function isPublished() {
      return (bool) $this->getEntityKey('status');
    }
  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($published) {
    // $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    return $this->get('tags')->value;
  }

    /**
   * {@inheritdoc}
   */
    public function getRelatedLmsPublications() {
      return $this->get('related_lmspublications')->value;
    }

      /**
   * {@inheritdoc}
   */
      public function getOwnerName() {
        $author = $this->get('uid')->entity->name->value;
        $author = ($author != '')?$author:'Anonymous';
        return $author;
      }

          /**
   * Generate array to export data
   * @return
   */
          function getExcelData() {
            $data['Title'] = $this->getTitle();
            $CreatedTime = $this->getCreatedTime();
            $ChangedTime = $this->getChangedTime();

            $ChangedTime = \Drupal::service('date.formatter')->format($ChangedTime, 'custom', 'M d Y');
            $CreatedTime = \Drupal::service('date.formatter')->format($CreatedTime, 'custom', 'M d Y');

            $data['ChangedTime'] = $ChangedTime;
            $data['CreatedTime'] = $CreatedTime;

            return $data;
          }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('LmsPublication ID'))
    ->setDescription(t('The ID of the lmspublication.'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
    ->setLabel(t('UUID'))
    ->setDescription(t('The lmspublication UUID.'))
    ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Title'))
    ->setDescription(t('The lmspublication title.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', array(
      'type' => 'string_textfield',
      'weight' => 0,
    ))
    ->setDisplayConfigurable('form', TRUE);


    $fields['featured_image'] = BaseFieldDefinition::create('image')
    ->setLabel(t('Featured Image'))
    ->setDescription(t('Image field'))
    ->setSettings([
      'file_directory' => 'lms/[date:custom:Y]-[date:custom:m]',
      'alt_field_required' => FALSE,
      'file_extensions' => 'png jpg jpeg',
    ])
    ->setDisplayOptions('view', array(
      'label' => 'hidden',
      'type' => 'default',
      'weight' => 1,
    ))
    ->setDisplayOptions('form', array(
      'label' => 'hidden',
      'type' => 'image_image',
      'weight' => 1,
    ))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);



    $fields['text_long'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Details'))
    ->setDescription(t('Main content of the lmspublication.'))
    ->setDisplayOptions('view', [
      'label' => 'visible',
      'type' => 'text_default',
      'weight' => 2,
    ])
    ->setDisplayOptions('form', [
      'type' => 'text_textarea',
      'weight' => 2,
      'rows' => 6,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);


    $fields['langcode'] = BaseFieldDefinition::create('language')
    ->setLabel(t('Language code'))
    ->setDescription(t('The lmspublication language code.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('When the lmspublication was created, as a Unix timestamp.'));

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return array(\Drupal::currentUser()->id());
  }

  /**
   *
   * {@inheritdoc}
   */
  public static function sort($a, $b) {
    return strcmp($a->label(), $b->label());
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // codes here
  }
}
