<?php

namespace Drupal\library_management_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining LmsBookAuthor entities.
 *
 * @ingroup library_management_system
 */
interface LmsBookAuthorInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the LmsBookAuthor title.
   *
   * @return string
   *   Title of the LmsBookAuthor.
   */
  public function getTitle();

  /**
   * Sets the LmsBookAuthor title.
   *
   * @param string $title
   *   The LmsBookAuthor title.
   *
   * @return \Drupal\library_management_system\Entity\LmsBookAuthorInterface
   *   The called LmsBookAuthor entity.
   */
  public function setTitle($title);

  /**
   * Gets the LmsBookAuthor creation timestamp.
   *
   * @return int
   *   Creation timestamp of the LmsBookAuthor.
   */
  public function getCreatedTime();

  /**
   * Sets the LmsBookAuthor creation timestamp.
   *
   * @param int $timestamp
   *   The LmsBookAuthor creation timestamp.
   *
   * @return \Drupal\library_management_system\Entity\LmsBookAuthorInterface
   *   The called LmsBookAuthor entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the LmsBookAuthor published status indicator.
   *
   * Unpublished LmsBookAuthor are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the LmsBookAuthor is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a LmsBookAuthor.
   *
   * @param bool $published
   *   TRUE to set this LmsBookAuthor to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\library_management_system\Entity\LmsBookAuthorInterface
   *   The called LmsBookAuthor entity.
   */
  public function setPublished($published);

}
