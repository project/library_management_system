<?php

namespace Drupal\library_management_system;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the IssuedLmsBook entity.
 *
 * @see \Drupal\library_management_system\Entity\IssuedLmsBook.
 */
class IssuedLmsBookAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\library_management_system\Entity\IssuedLmsBookInterface $entity */

    switch ($operation) {
      case 'view':

      $owner_id = $entity->getOwnerId();
      $current_uid = $account->id();

      if($owner_id == $current_uid) {
        return AccessResult::allowed();
      } else {
        return AccessResult::allowedIfHasPermission($account, 'view published issuedlmsbook entities');
      }

      case 'update':
      return AccessResult::allowedIfHasPermission($account, 'edit issuedlmsbook entities');

      case 'delete':
      return AccessResult::allowedIfHasPermission($account, 'delete issuedlmsbook entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add issuedlmsbook entities');
  }

}
